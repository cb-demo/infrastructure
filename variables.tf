variable "project_name" {
  default = "cb-demo"
}

variable "region" {
  default = "us-west-2"
}

variable "ecr_image" {
  default = "registry.gitlab.com/dfh72/dotnet_web_app:latest"
}

variable "desired_count" {
  default = 1
}

variable "vpc_id" {
  default = "vpc-062c4b7f20fae16e7"
}
